-- MySQL dump 10.13  Distrib 8.0.20, for Linux (x86_64)
--
-- Host: localhost    Database: db_aucution
-- ------------------------------------------------------
-- Server version	8.0.20-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `category_id` int NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf16;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'cate 1'),(2,'cate 2'),(3,'cate 3'),(4,'cate 4'),(5,'cate 5'),(6,'cate 6');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `message` (
  `message_id` int NOT NULL AUTO_INCREMENT,
  `user_send` int NOT NULL,
  `user_give` int NOT NULL,
  `time_send` bigint NOT NULL,
  `content` varchar(1000) NOT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `product_id` int NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) DEFAULT NULL,
  `user_id` int NOT NULL,
  `category_id` int NOT NULL,
  `price` bigint NOT NULL,
  `information` varchar(1000) NOT NULL,
  `image1` varchar(255) NOT NULL,
  `image2` varchar(255) DEFAULT NULL,
  `image3` varchar(255) DEFAULT NULL,
  `time_up` bigint NOT NULL,
  `is_ok` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_aucution`
--

DROP TABLE IF EXISTS `product_aucution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_aucution` (
  `aucutionn_id` int NOT NULL AUTO_INCREMENT,
  `aucution_name` varchar(255) NOT NULL,
  `user_id` int NOT NULL,
  `category_id` int NOT NULL,
  `information` varchar(1000) NOT NULL,
  `image1` varchar(255) NOT NULL,
  `image2` varchar(255) DEFAULT NULL,
  `image3` varchar(255) DEFAULT NULL,
  `time_up` bigint NOT NULL,
  `time_down` bigint NOT NULL,
  `is_ok` int DEFAULT '0',
  PRIMARY KEY (`aucutionn_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf16;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_aucution`
--

LOCK TABLES `product_aucution` WRITE;
/*!40000 ALTER TABLE `product_aucution` DISABLE KEYS */;
INSERT INTO `product_aucution` VALUES (1,'dsad',6,4,'okok','1591028457102-aa56bba85e5ba505fc4a.jpg',NULL,NULL,1591028459253,1591698555555,1),(2,'aucution 2',8,2,'dasd','dasd','das','das',150000000,1591699897654,1),(3,'aucution name of my',6,3,'what the fuck','1591695527543-logohust.png','1591695527544-DM.png',NULL,1591695527545,1591698564345,0);
/*!40000 ALTER TABLE `product_aucution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET utf16 COLLATE utf16_general_ci NOT NULL,
  `password` varchar(20) CHARACTER SET utf16 COLLATE utf16_general_ci NOT NULL,
  `full_name` varchar(255) CHARACTER SET utf16 COLLATE utf16_general_ci DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `auth` int DEFAULT '0',
  `avatar` varchar(255) NOT NULL DEFAULT 'avartar.png',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (6,'admin','123456','quang hiếu aaa','0869175144','17 ngách 152 ngõ 141 giáp nhị',1,'1591695394455-logohust.png'),(7,'username','123456','đỗ hiếu','0869175144','hoàng mai, hà nội',0,'avatar.png'),(8,'kkviphieu1','123456','quang hiếu','undefined','17 ngách 152 ngõ 141 giáp nhị,0869175144',0,'avatar.png'),(9,'kkviphieu12','123456','quang hiếu','undefined','17 ngách 152 ngõ 141 giáp nhị,0869175144',0,'avatar.png'),(10,'user2','123456','quang hiếu 2','0869175144','17 ngách 152 ngõ 141 giáp nhị',0,'avatar.png'),(11,'user3','123456','quang hiếu 3','0869175144','17 ngách 152 ngõ 141 giáp nhị',0,'avatar.png'),(12,'user5','123456','quang hiếu 5','0869175144','17 ngách 152 ngõ 141 giáp nhị',0,'avatar.png'),(13,'user6','123456','quang hiếu 6','0869175144','17 ngách 152 ngõ 141 giáp nhị',0,'avatar.png'),(14,'user7','123456','quang hiếu7 ','0869175144','17 ngách 152 ngõ 141 giáp nhị',0,'avatar.png');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_aucution`
--

DROP TABLE IF EXISTS `user_aucution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_aucution` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `aucution_id` int DEFAULT NULL,
  `time_buy` bigint DEFAULT NULL,
  `price` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_aucution`
--

LOCK TABLES `user_aucution` WRITE;
/*!40000 ALTER TABLE `user_aucution` DISABLE KEYS */;
INSERT INTO `user_aucution` VALUES (1,6,1,15600000,'15000'),(2,7,1,15600000,'1500000'),(3,6,2,123455,'123455'),(4,7,2,2182882,'12312312');
/*!40000 ALTER TABLE `user_aucution` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-09 22:33:25
