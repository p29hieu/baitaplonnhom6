function settingCallEvent(call1, localVideo, remoteVideo, callButton, answerCallButton, endCallButton, rejectCallButton, ChangeVideoCallButton) {
    call1.on('addremotestream', function (stream) {
        // reset srcObject to work around minor bugs in Chrome and Edge.
        console.log('addremotestream', stream);
        if (stream.getVideoTracks()[0].enabled) {
            $('#video-container').show();
        }
        remoteVideo.srcObject = null;
        remoteVideo.srcObject = stream;
    });

    call1.on('addlocalstream', function (stream) {
        // reset srcObject to work around minor bugs in Chrome and Edge.
        console.log('addlocalstream', stream);
        localVideo.srcObject = null;
        localVideo.srcObject = stream;
    });

    call1.on('signalingstate', function (state) {
        console.log('signalingstate ', state);

        if (state.code == 6 || state.code == 5)//end call or callee rejected
        {
            callButton.show();
            endCallButton.hide();
            ChangeVideoCallButton.hide()
            rejectCallButton.hide();
            answerCallButton.hide();
            localVideo.srcObject = null;
            remoteVideo.srcObject = null;
            $('#video-container').hide();
            $('#incoming-call-notice>h3').text(calleeId + " " + state.reason);
            $('#incoming-call-notice>div').hide();
        }
        else {
            // 
            $('#incoming-call-notice>h3').text("you and " + calleeId + " in a call.");
        }
    });

    call1.on('mediastate', function (state) {
        console.log('mediastate ', state);
        if (state.code == 1) {
            ChangeVideoCallButton.show()
        }
        else ChangeVideoCallButton.hide()
    });
    call1.on('info', function (info) {
        console.log('on info:' + JSON.stringify(info));
    });
}

jQuery(async function () {
    var localVideo = document.getElementById('localVideo');
    var remoteVideo = document.getElementById('remoteVideo');
    var callButton = $('#callButton');
    var answerCallButton = $('#answerCallButton');
    var rejectCallButton = $('#rejectCallButton');
    var endCallButton = $('#endCallButton');
    var ChangeVideoCallButton = $('#ChangeVideoCallButton');
    var currentCall = null;
    var client = new StringeeClient();
    var getToken = await $.ajax({
        url: "/videocall/getAccessToken/" + callerId+"?new=0",
        type: 'GET',
        success: function (res) {
            if (res) {
                if (res.error) {
                    alert(error)
                }
                else {
                    return res
                }
            }
        },
        error: function (e) {
            console.log(e.message);
        }
    });
    client.connect(getToken.token);

    client.on('connect', function () {
        console.log('+++ connected!');
    });
    client.on('authen', async function (res) {
        console.log('+++ on authen: ', res);
        if (res.r == 2) {
            console.log('++++++++++++++ requestnewtoken; please get new access_token from YourServer and call client.connect(new_access_token)+++++++++');
            //please get new access_token from YourServer and call: 
            var new_access_token = await $.ajax({
                url: "/videocall/getAccessToken/" + callerId+"?new=1",
                type: 'GET',
                success: function (res) {
                    if (res) {
                        if (res.error) {
                            alert(error)
                        }
                        else {
                            console.log("get token", res.token);
                            return res.token
                        }
                    }
                },
                error: function (e) {
                    console.log(e.message);
                }
            });
            client.connect(new_access_token.token);
        }
    });

    client.on('disconnect', function (res) {
        console.log('+++ disconnected', res);
        $('#video-container').hide();
        $('#incoming-call-notice>h3').text("DISCONNECTED")
        $('#incoming-call-notice>div').hide()
    });

    //MAKE CALL
    callButton.on('click', function () {
        currentCall = new StringeeCall(client, callerId, calleeId, false);

        settingCallEvent(currentCall, localVideo, remoteVideo, callButton, answerCallButton, endCallButton, rejectCallButton, ChangeVideoCallButton);

        currentCall.makeCall(function (res) {
            console.log('+++ call callback: ', res);
            if (res.message === 'SUCCESS') {
                document.dispatchEvent(new Event('connect_ok'));
            }
        });
        $('#incoming-call-notice').show()
        $('#incoming-call-notice>h3').text("You are calling " + calleeId)
    });

    //RECEIVE CALL
    client.on('incomingcall', function (incomingcall) {
        console.log("+++ incomnging call", incomingcall);
        $('#incoming-call-notice').show();
        $('#incoming-call-notice>h3').text(calleeId + " calling you...");
        currentCall = incomingcall;
        settingCallEvent(currentCall, localVideo, remoteVideo, callButton, answerCallButton, endCallButton, rejectCallButton, ChangeVideoCallButton);
        callButton.hide();
        answerCallButton.show();
        rejectCallButton.show();
    });

    //Event handler for buttons
    answerCallButton.on('click', function () {
        rejectCallButton.hide();
        endCallButton.show();
        ChangeVideoCallButton.show();
        callButton.hide();
        console.log('current call ', currentCall);
        if (currentCall != null) {
            currentCall.answer(function (res) {
                console.log('+++ answering call: ', res);
            });
        }
        $('#incoming-call-notice>h3').text("You and " + calleeId + " in a call.")
        answerCallButton.hide();
    });

    rejectCallButton.on('click', function () {
        if (currentCall != null) {
            currentCall.reject(function (res) {
                console.log('+++ reject call: ', res);
            });
        }
        $('#video-container').hide();
        $('#incoming-call-notice').hide();
        answerCallButton.hide();
        ChangeVideoCallButton.hide();
        callButton.show();
        rejectCallButton.hide();
        $(this).hide();
    });

    endCallButton.on('click', function () {
        if (currentCall != null) {
            currentCall.hangup(function (res) {
                console.log('+++ hangup: ', res);
            });
        }
        $('#video-container').hide();
        $('#incoming-call-notice').hide();
        callButton.show();
        ChangeVideoCallButton.hide();
        answerCallButton.hide();
        endCallButton.hide();
    });

    ChangeVideoCallButton.on('click', function () {
        $('#video-container').show();
        currentCall.upgradeToVideoCall();
    });
    //event listener to show and hide the buttons
    document.addEventListener('connect_ok', function () {
        callButton.hide();
        endCallButton.show();
    });
});