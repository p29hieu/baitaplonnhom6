require("dotenv").config();
const mysql = require("mysql");
var express = require("express");
var router = express.Router();
const conn = mysql.createConnection({
  host: process.env.DB_HOST || "localhost",
  user: process.env.DB_USER || "test",
  password: process.env.DB_PASSWORD || "123456",
  database: process.env.DB_DATABASE || "db_aucution",
});
//kết nối vào db; nếu đc thông báo connect ok
conn.connect(function (err) {
  err ? console.log(err) : console.log(new Date(), "[Connect OK!]");
});
module.exports = conn;
