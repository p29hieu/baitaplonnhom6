const session = require("express-session");

const conn = require("./connection");
var createError = require("http-errors");
const express = require("express"),
  es6Renderer = require("express-es6-template-engine"),
  app = express();
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var bodyParser = require("body-parser");
//set up các biến file js
var loginRouter = require("./routes/login");
var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var registerRouter = require("./routes/register");
var getImageRouter = require("./routes/getImage");
var logoutRouter = require("./routes/logout");
var adminRouter = require("./routes/admin");
var categoryRouter = require("./routes/categoryProduct");
var aucutionRouter = require("./routes/aucution")
require("dotenv").config();
app.use(express.static("public"));

// view engine setup

// app.engine('html', es6Renderer);
app.set("views", "views");
app.set("view engine", "ejs");
//lấy file jpg; ảnh; ...
myLogger = (req, res, next) => {
  s = req.url.search('css') && req.url.search('/images')
  if (s)
    console.log(`${req.method} ${req.url}`);
  next()
}
app.use(myLogger);
//sd session
app.use(
  session({
    resave: true,
    saveUninitialized: true,
    secret: "somesecret",
    cookie: {
      secure: false, //  sửa thành true để sử dụng https
      maxAge: 6000000,
    },
  })
);
//nodejs sd json; mã hóa url cho body parse
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

// get all category
getCategories = (req, res, next) => {
  if (!req.session.category) {
    sql = `SELECT * FROM db_aucution.category;`;
    conn.query(sql, [], (err, result, field) => {
      if (err) {
        res.render("error", { error: err }); //render ra lỗi
        throw err;
      } else {
        req.session.category = result;
      }
    });
  }
  if(!req.session.search) req.session.search = {}
  if (!req.session.search.selected_category_id) req.session.search.selected_category_id = -1;
  if (!req.session.search.aucution_selected_category_id) req.session.search.aucution_selected_category_id = -1;
  if(!req.session.search.minPrice) req.session.search.minPrice = 0
  if(!req.session.search.maxPrice) req.session.search.maxPrice = 10000000000
  if (!req.session.user) req.session.user = null;
  next();
};
app.use(getCategories);

app.use("/", indexRouter); // trang chủ
app.use("/login", loginRouter); // đăng nhập
app.use("/register", registerRouter); // đăng kí
app.use("/logout", logoutRouter); // đăng xuất
app.use("/images", getImageRouter); //lấy ảnh

app.use("/admin", adminRouter); // quyền admin với tài khoản admin

app.use("/category", categoryRouter); // xem sản phẩm theo category
app.use("/users", usersRouter); // quyền user
//set up biến file js; sau đó file js này render
var single = require("./routes/single"); ////////////////////////////
// app.use("/single", function (req, res, next) {});
app.use("/single", single); ////////////////////



app.use("/aucution", aucutionRouter); // đấu giá

const videoCall = require('./routes/videoCall')
app.use("/videocall", videoCall); // goi video


app.get("/icons", (req, res, next)=>{
  res.render('icons')
}); ////////////////////
app.get("/contact", (req, res, next)=>{
  res.render('contact')
}); ////////////////////


var chatbox = require('./routes/chatbox')
app.use('/chatbox', chatbox) // chatbox


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
// app.use(function
he = (err, req, res, next) => {
  // tương tự arow fun; nhưng viết r gọi luôn
  // set locals, only providing error in development
  //có lỗi nó sẽ tắt run
  res.locals.message = err.message;
  console.log(err);
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error", { locals: { error: err } });
  // });
};
app.use(he);

module.exports = app;
