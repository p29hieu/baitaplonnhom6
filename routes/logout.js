var express = require('express');
var router = express.Router();
const conn = require('../connection')
//logout
router.get('/', function (req, res, next) {
    if (req.session.user) {
        delete req.session.user
        res.redirect('/')
    }
    else res.render('error', {
        error: true,
        message: "[Your not login yet]"
    })
})

module.exports = router;