var express = require("express");
var router = express.Router();
const conn = require("../connection");

/* GET home page. */

// xem tất cả sản phẩm
router.get("/", function (req, res, next) {
  sql = `SELECT 
    p.*, u.username, u.full_name user_full_name,u.phone, u.address, c.category_name
FROM
    product p,
    user u,
    category c
WHERE
    p.user_id = u.user_id
    AND c.category_id = p.category_id   
    AND p.is_ok =1
ORDER BY p.time_up ASC;`

  conn.query(sql, [], (err, result, field) => {
    if (err) {
      res.render("error", { error: true, message: err });
      throw err;
    } else {
      console.log(req.app.get("message") || "[Get all product OK]");
      if(!req.session.cate) req.session.cate={}
      req.session.search.selected_category_id= -1
      res.render("index", {
        ...req.session.search,
        category: req.session.category,
        result: result,
        num: result.length,
        message: req.app.get("message") || "[Get all product OK]",
        user: req.session.user || null,
        serverTime: new Date().getTime()
      });
    }
  });
});
// tìm kiếm sản phẩm
router.post('/search', (req, res, next) => {
    var sql = `SELECT 
    p.*, u.username, u.full_name user_full_name,u.phone, u.address, c.category_name
FROM
    product p,
    user u,
    category c
WHERE
    p.user_id = u.user_id
    AND c.category_id = p.category_id   
    AND p.is_ok ='1'
    AND price >= ${req.body.minPrice||0}
    AND price <= ${req.body.maxPrice||1000000000} `
    if (req.session.search.selected_category_id>0) sql += `AND p.category_id = ${req.session.search.selected_category_id} `
    sql += `ORDER BY p.time_up ASC;`

    conn.query(sql, [], (err, result, field) => {
        if (err) {
            res.render('error', { error: true, message: err })
            throw err
        }
        else {
          req.session.search.minPrice = req.body.minPrice||0
          req.session.search.maxPrice = req.body.maxPrice||1000000000
            console.log("[SEARCH PRODUCT OK]: result.length: "+result.length)
            res.render('index', {
                ...req.session.search,
                category: req.session.category,
                result: result,
                user: req.session.user,
                num: result.length,
                message: "[Search PRODUCT OK]",
        serverTime: new Date().getTime()
            });
        }
    })
})


router.get("/servertime",(req,res,next)=>{
  res.send({
    serverTime: new Date().getTime()
  })
})
module.exports = router;
