var express = require('express');
var router = express.Router();
const conn = require('../connection')
// check login
router.use((req, res, next) => {
    if (req.session.user == null) {
        res.render('error',
            {
                error: true,
                message: "[YOU MUST LOGGIN FIRST]"
            }
        )
    }
    else
        next()
})

router.get("/", (req, res, next) => {
    var sql = `
SELECT 
    p.*,
    u.username,
    u.full_name user_full_name,
    u.phone,
    u.address,
    c.category_name,
    MAX(ua.price) price_max
FROM
    product_aucution p,
    user u,
    category c,
    user_aucution ua
WHERE
    p.user_id = u.user_id
        AND c.category_id = p.category_id
        AND p.is_ok = 1
        AND ua.aucution_id = p.aucutionn_id
group by ua.aucution_id
ORDER BY p.time_down ASC;`

    conn.query(sql, [], (err, result, field) => {
        if (err) {
            res.render("error", { error: true, message: err });
            throw err;
        } else {
            console.log(req.app.get("message") || "[Get all product OK]");
            req.session.search.minPrice = 0
            req.session.search.maxPrice = 0
            req.session.search.selected_category_id = -1
            req.session.search.aucution_selected_category_id = -1
            res.render("auindex", {
                ...req.session.search,
                category: req.session.category,
                result: result,
                num: result.length,
                message: req.app.get("message") || "[Get all product OK]",
                user: req.session.user,
                serverTime: new Date().getTime()
            });
        }
    });
})

// xem sản phẩm theo category
router.get('/category/:cID', function (req, res, next) {
    var cID = req.params.cID
    var sql = `SELECT 
    p.*,
    u.username,
    u.full_name user_full_name,
    u.phone,
    u.address,
    c.category_name,
    MAX(ua.price) price_max
FROM
    product_aucution p,
    user u,
    category c,
    user_aucution ua
WHERE
    p.user_id = u.user_id
        AND c.category_id = p.category_id
        AND p.is_ok = 1
        AND ua.aucution_id = p.aucutionn_id
        AND p.category_id = '${cID}'
group by ua.aucution_id
ORDER BY p.time_down ASC;`
    conn.query(sql, [], (err, result, field) => {
        if (err) {
            res.send({
                error: true,
                message: err
            })
            throw err
        }
        else {
            console.log("[Get category aucution OK], id: " + cID);
            req.session.search.aucution_selected_category_id = cID
            res.render('auindex', {
                ...req.session.search,
                category: req.session.category,
                user: req.session.user,
                result: result,
                num: result.length,
                message: "[Get category aucution OK], id: " + cID,
                serverTime: new Date().getTime()
            });
        }
    })
});

// tìm kiếm sản phẩm
router.post('/search', (req, res, next) => {
    var category = null
    if (req.session.aucution_selected_category_id != -1)
        category = req.session.aucution_selected_category_id

    var sql = `SELECT 
    p.*,
    u.username,
    u.full_name user_full_name,
    u.phone,
    u.address,
    c.category_name,
    MAX(ua.price) price_max
FROM
    product_aucution p,
    user u,
    category c,
    user_aucution ua
WHERE
    p.user_id = u.user_id
        AND c.category_id = p.category_id
        AND p.is_ok = 1
        AND ua.aucution_id = p.aucutionn_id
    AND price >= ${req.body.minPrice}
    AND price <= ${req.body.maxPrice}
group by ua.aucution_id
ORDER BY p.time_down ASC;`


    if (category) sql += `AND p.category_id = ${category} `
    sql += `ORDER BY p.time_down DESC;`

    conn.query(sql, [], (err, result, field) => {
        if (err) {
            res.render('error', { error: true, message: err })
            throw err
        }
        else {
            console.log("[SEARCH AUTION OK]")
            res.render('auindex', {
                ...req.session.cate,
                result: result,
                user: req.session.user,
                num: result.length,
                message: "[Search aucution OK]",
                serverTime: new Date().getTime()
            });
        }
    })
})
router.get("/singleaucution/:cID", (req, res, next) => {
    var sql = `SELECT 
    p.*, u.username, u.full_name user_full_name,u.phone, u.address, c.category_name
FROM
    product_aucution p,
    user u,
    category c
WHERE
    p.user_id = u.user_id
    AND c.category_id = p.category_id   
    AND p.aucution_id = ${cID}
    AND p.is_ok =1
ORDER BY p.time_up ASC;`

    conn.query(sql, [], (err, result, field) => {
        if (err) {
            res.render("error", { error: true, message: err });
            throw err;
        } else {
            console.log(req.app.get("message") || "[Get all product OK]");
            req.session.search.minPrice = 0
            req.session.search.maxPrice = 0
            req.session.search.selected_category_id = -1
            req.session.search.aucution_selected_category_id = -1
            res.render("singleacution", {
                ...req.session.search,
                category: req.session.category,
                result: result,
                num: result.length,
                message: req.app.get("message") || "[Get all product OK]",
                user: req.session.user,
            });
        }
    });
})
module.exports = router