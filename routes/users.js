var express = require('express');
const conn = require('../connection')
var router = express.Router();
var formidable = require('formidable');
var fs = require('fs');
const getAccessToken = require("../accessToken/access_token_for_client")

var forEach = require('../forEach')
// kiểm tra quyền
router.use((req, res, next) => {
  if (req.session.user) {
    if (!req.session.search.user) {
      req.session.search.user = req.session.user
    }
    next()
  }
  else {
    return res.render("error", {
      error: true,
      message: "[You must login first!]"
    })
  }
})


// uploadfile
uploadFile = async (file) => {
  if (!file.size) return null
  let math = ["image/png", "image/jpeg"];
  if (math.indexOf(file.type) === -1)
    return null

  var oldpath = file.path;
  var t = new Date().getTime()
  var newFileName = new Date().getTime() + "-" + file.name
  var newpath = './images/' + newFileName;
  fs.rename(oldpath, newpath, (err) => {
    if (err) {
      console.log("[upFile Error]", err)
      return false
    }
  });
  return newFileName
}
const upFile = async (req, res, next) => {
  var form = new formidable.IncomingForm();
  var data = []
  await form.parse(req, async (err, fields, files) => {
    if (err) {
      res.render('error', {
        error: true,
        message: err
      })
      throw err
    }
    if (files) {
      await forEach(files, async (val, prop, obj) => {
        let result = await uploadFile(val)
        if (result) {
          data.push(result)
        }
      })
      req.body = {
        ...fields,
        image1: data[0] || null,
        image2: data[1] || null,
        image3: data[2] || null,
      }
      next()
    }
  })
}
/* GET users */
// xem user
router.get('/:uID', function (req, res, next) {
  let uID = req.params.uID
  sql = `Select * from user where user_id = '${uID}';`
  conn.query(sql, [], (err, result, field) => {
    if (err) {
      res.send({
        error: true,
        message: err
      })
      throw err
    }
    else if (result.length) {
      delete result[0].password
      console.log("[Get user OK]: " + uID)
      req.session.search = {
        ...req.session.search,
        user: result[0]
      }
      var token = getAccessToken(uID)
      console.log("[token for " + uID + "]: ", token);
      res.render("user/userProfile", {
        error: false,
        message: "[Get user OK]: " + uID,
        search: {
          ...req.session.search
        },
        user: req.session.user,
        type: 1,
        token: token
      })
    }
    else res.render("error", {
      error: true,
      message: "[No user id] " + req.params.uID
    })
  })
});
// xóa người dùng
router.post('/delete', (req, res) => {
  if (req.session.user.user_id != req.body.user_id || req.session.user.auth != '1') {
    return res.render('error', {
      error: true,
      message: "You do not permission!!",
    })
  }
  sql = `DELETE FROM \`user\` WHERE (\`user_id\` = '${req.params.userID}');`
  conn.query(sql, [], (err, result, field) => {
    if (err) {
      res.send({
        error: true,
        message: err
      })
      throw err
    }
    else {
      console.log("[DELETE USER OK]");
      res.send({
        error: false,
        message: "[DELETE USER OK]"
      })
    }
  })
})

// 4.sửa người dùng

router.get("/change/user", (req, res, next) => {
  res.render('user/userChangeInformation', {
    user: req.session.user,
    search: req.session.search,
    message: "Change My Information",
    type: 4,
  })
})
// form xử lý sửa người dùng
router.post('/change/user', async (req, res, next) => {
  var form = new formidable.IncomingForm();
  var data = []
  // const form = formidable({ multiples: true });
  await form.parse(req, async (err, fields, files) => {
    if (err) {
      res.render('error', {
        error: true,
        message: err
      })
      throw err
    }
    if (files) {
      await forEach(files, async (val, prop, obj) => {
        let result = await uploadFile(val)
        if (result) {
          data.push(result)
        }
      })
      req.body = {
        ...fields,
        image1: data[0] || null,
      }
      next()
    }
  })
})
router.post('/change/user', (req, res) => {
  if (req.session.user.user_id != req.body.user_id) {
    return res.render("error", {
      error: true,
      message: "[You do not permission]"
    })
  }
  else {
    var u = {
      oldPassword: req.body.oldPassword,
      password: req.body.password,
      full_name: req.body.full_name,
      phone: req.body.phone,
      address: req.body.address,
      avt: req.body.image1
    }
    if (!u.password || !u.password.length) delete u.password
    if (u.oldPassword != req.session.user.password) {
      res.render('user/userChangeInformation', {
        user: req.session.user,
        search: req.session.search,
        message: "Your password is wrong!",
        type: 4
      })
      return
    }
    var sql = `UPDATE \`user\` SET `
    if (u.password)
      sql += ` \`password\` = '${u.password}', `
    if (u.full_name)
      sql += ` \`full_name\` = '${u.full_name}', `
    if (u.phone)
      sql += ` \`phone\` = '${u.phone}', `
    if (u.address)
      sql += ` \`address\` = '${u.address}', `
    if (u.avt)
      sql += ` \`avatar\` = '${u.avt}', `
    if (u.username)
      sql += ` \`username\` = '${u.username}' `
    else
      sql += ` \`username\` = '${req.session.user.username}' `
    sql += ` WHERE(\`user_id\` = '${req.session.user.user_id}');`
    conn.query(sql, [], (err, result) => {

      if (err) {
        res.render("error", {
          error: true,
          message: err
        })
      }
      else {
        req.session.user = {
          ...req.session.user,
          ...u
        }
        res.redirect("/users/" + req.session.user.user_id)
      }
    })
  }
})
// end sửa người dùng


// xem sản phẩm của user chưa được chấp nhận
router.get('/product/notacept/:uID', (req, res) => {

  let sql = `SELECT 
    p.*, u.username, u.full_name user_full_name,u.phone, u.address, c.category_name
FROM
    product p,
    user u,
    category c
WHERE
    p.user_id = u.user_id
    AND c.category_id = p.category_id   
    AND p.is_ok ='0'
    AND u.user_id = '${req.params.uID}'
ORDER BY p.time_up ASC;`
  conn.query(sql, [], (err, result) => {
    if (err) {
      res.send({
        error: true,
        message: err
      })
      throw err
    }
    req.session.search = {
      ...req.session.search,
      user: { user_id: req.params.uID }
    }
    return res.render('user/userProduct', {
      search: {
        ...req.session.search
      },
      user: req.session.user,
      error: false,
      message: "[Get product user ok]",
      result: result,
      serverTime: new Date().getTime(),
      type: 3
    })
  })
})
// xem sản phẩm của user đã được chấp nhận
router.get('/product/acept/:uID', (req, res) => {
  let sql = `SELECT 
    p.*, u.username, u.full_name user_full_name,u.phone, u.address, c.category_name
FROM
    product p,
    user u,
    category c
WHERE
    p.user_id = u.user_id
    AND c.category_id = p.category_id   
    AND p.is_ok ='1'
    AND u.user_id = '${req.params.uID}'
ORDER BY p.time_up ASC;`
  conn.query(sql, [], (err, result) => {
    if (err) {
      res.send({
        error: true,
        message: err
      })
    }
    else {
      req.session.search = {
        ...req.session.search,
        user: { user_id: req.params.uID }
      }
      return res.render('user/userProduct', {
        search: {
          ...req.session.search
        },
        user: req.session.user,
        error: false,
        message: "[Get product user ok]",
        result: result,
        serverTime: new Date().getTime(),
        type: 2
      })
    }
  })
})
// xóa sản phẩm
router.post('/product/delete/:pID', (req, res) => {
  sql = `DELETE FROM \`product\` WHERE (\`user_id\` = '${req.session.user.user_id}' and \`product_id\` = '${req.params.pID}');`
  conn.query(sql, [], (err, result, field) => {
    if (err) {
      res.render("error", {
        error: true,
        message: err
      })
    }
    else {
      console.log("[DELETE OK]");
      res.redirect('/users/product/' + req.session.user.user_id)
    }
  })
})

//sửa sản phẩm
router.put('/product/:pID', (req, res) => {
  p = req.body
  sql = `UPDATE \`db_aucution\`.\`product\` SET `
  if (p.product_name)
    sql += ` \`product_name\` = '${p.product_name}', `
  if (p.category_id)
    sql += ` \`category_id\` = '${p.category_id}', `
  if (p.price)
    sql += ` \`price\` = '${p.price}', `
  if (p.infomation)
    sql += ` \`infomation\` = '${p.infomation}', `
  if (p.image1)
    sql += ` \`image1\` = '${p.image1}', `
  if (p.image2)
    sql += ` \`image2\` = '${p.image2}', `
  if (p.image3)
    sql += ` \`image3\` = '${p.image3}', `
  sql += ` \`time_up\` = '${new Date().getTime()}' `
  sql += ` WHERE(\`product_id\` = '${req.params.pID}' and \`user_id\` = '${req.session.user.user_id}');`
  conn.query(sql, [], (err, result, field) => {
    if (err) {
      console.log(err);
      res.render('error', {
        error: true,
        message: err
      })
    }
    else {
      res.send({
        error: false,
        message: "[UPDATE PRODUCT OK]"
      })
    }
  })
})
//thêm sản phẩm
router.get('/insert/product', (req, res) => {
  res.render('user/userInsertProduct', {
    user: req.session.user,
    search: req.session.search,
    message: "INSERT PRODUCT NOW",
    category: req.session.category,
    serverTime: new Date().getTime(),
    type: 5
  })
})
// form xử lý thêm sản phẩm
router.post('/insert/product', (req, res, next) => upFile(req, res, next))
router.post('/insert/product', (req, res) => {
  p = req.body
  sql = " INSERT INTO product (`product_name`, `user_id`, `category_id`, `price`, `information`, `image1`, `image2`, `image3`, `time_up`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);"
  p_insert = {
    product_name: p.product_name || null,
    user_id: req.session.user.user_id,
    category_id: p.category_id || 1,
    price: p.price || 1,
    information: p.information || "",
    image1: p.image1 || "",
    image2: p.image2 || null,
    image3: p.image3 || null,
    time_up: new Date().getTime()
  }
  values = Object.values(p_insert)
  conn.query(sql, values, (err, result, field) => {
    if (err) {
      console.log(err);

      res.render("error", {
        error: true,
        message: err
      })
    }
    else {
      console.log("[INSERT PRODUCT OK]");
      res.redirect('/users/product/notacept/' + req.session.user.user_id)
    }
  })
})


// đấu giá 

// 6. xem các sản phẩm đang đấu giá 
router.get('/aucution/:uID', (req, res) => {
  let sql = `SELECT 
    p.*,
    u.username,
    u.full_name user_full_name,
    u.phone,
    u.address,
    c.category_name,
    MAX(ua.price) price_max
FROM
    product_aucution p,
    user u,
    category c,
    user_aucution ua
WHERE
    p.user_id = u.user_id
        AND c.category_id = p.category_id
        AND p.is_ok = 1
        AND ua.aucution_id = p.aucutionn_id
        AND p.user_id = '${req.params.uID}'
group by ua.aucution_id
ORDER BY p.time_down ASC;`
  conn.query(sql, [], (err, result) => {
    if (err) {
      res.render('error', {
        error: true,
        message: err
      })
    }
    else {
      req.session.search = {
        ...req.session.search,
        user: { user_id: req.params.uID }
      }
      console.log("[Get product_aucution user ok], length: ", result.length);
      return res.render('user/userProduct', {
        search: {
          ...req.session.search
        },
        user: req.session.user,
        error: false,
        message: "[Get product_aucution user ok]",
        result: result,
        type: 6,
        serverTime: new Date().getTime()
      })
    }
  })
})
// 7. Xem các sản phẩm đang chờ duyệt để đấu giá (chỉ của user)
router.get('/not/aucution', (req, res) => {
  let sql = `SELECT 
    p.*,
    u.username,
    u.full_name user_full_name,
    u.phone,
    u.address,
    c.category_name,
    MAX(ua.price) price_max
FROM
    product_aucution p,
    user u,
    category c,
    user_aucution ua
WHERE
    p.user_id = u.user_id
        AND c.category_id = p.category_id
        AND p.is_ok = 0
        AND ua.aucution_id = p.aucutionn_id
        AND p.user_id = '${req.session.user.user_id}'
group by ua.aucution_id
ORDER BY p.time_down ASC;`
  conn.query(sql, [], (err, result) => {
    if (err) {
      res.render('error', {
        error: true,
        message: err
      })
    }
    else {
      console.log("[Get product_aucution not ok], length: ", result.length);
      return res.render('user/userProduct', {
        search: {
          ...req.session.search
        },
        user: req.session.user,
        error: false,
        message: "[Get product_aucution not ok]",
        result: result,
        type: 7,
        serverTime: new Date().getTime()
      })
    }
  })
})

// 8. xem các sản phẩm đã hết hạn đấu giá /users/done/aucution
router.get('/done/aucution', (req, res) => {

  var sql = `SELECT 
    p.*,
    u.username,
    u.full_name user_full_name,
    u.phone,
    u.address,
    c.category_name,
    MAX(ua.price) price_max
FROM
    product_aucution p,
    user u,
    category c,
    user_aucution ua
WHERE
    p.user_id = u.user_id
        AND c.category_id = p.category_id
        AND p.is_ok = 1
        AND ua.aucution_id = p.aucutionn_id
    AND u.user_id = '${req.session.user.user_id}'
    AND time_down<=${new Date().getTime()}
group by ua.aucution_id
ORDER BY p.time_down ASC;`
  conn.query(sql, [], (err, result) => {
    if (err) {
      res.send({
        error: true,
        message: err
      })
    }
    else {
      console.log("[Get product_aucution done ok]. result.length=" + result.length);
      return res.render('user/userProduct', {
        search: {
          ...req.session.search
        },
        user: req.session.user,
        error: false,
        message: "[Get product_aucution done ok]",
        result: result,
        type: 8,
        serverTime: new Date().getTime(),
      })
    }

  })
})

// 9. Thêm sản phẩm đấu giá /users/insert/aucution
router.get('/insert/aucution', (req, res) => {
  res.render('user/userInsertAucion', {
    user: req.session.user,
    search: req.session.search,
    message: "INSERT AUCUTION NOW",
    category: req.session.category,
    type: 9,
    serverTime: new Date().getTime(),
  })
})
// form thêm aucution
router.post('/insert/aucution', (req, res, next) => upFile(req, res, next))
router.post('/insert/aucution', (req, res) => {
  p = req.body
  time_down = new Date().getTime()
  if (parseInt(p.day)) time_down += parseInt(p.day) * 3600 * 24
  if (parseInt(p.hours)) time_down += parseInt(p.hours) * 3600
  sql = " INSERT INTO product_aucution (`aucution_name`, `user_id`, `category_id`, `price`, `information`, `image1`, `image2`, `image3`, `time_up`, `time_down`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?,?);"
  p_insert = {
    aucution_name: p.aucution_name || "",
    user_id: req.session.user.user_id,
    category_id: p.category_id || 1,
    price: p.price || 1,
    information: p.information || "",
    image1: p.image1 || "",
    image2: p.image2 || null,
    image3: p.image3 || null,
    time_up: new Date().getTime(),
    time_down: time_down
  }
  values = Object.values(p_insert)
  conn.query(sql, values, (err, result, field) => {
    if (err) {
      res.render("error", {
        error: true,
        message: err
      })
      throw err
    }
    else {
      console.log("[INSERT AUCUTION OK], id: ");
      res.redirect('/users/aucution/' + req.session.user.user_id + "?is_ok=0")
    }
  })
})


module.exports = router;
