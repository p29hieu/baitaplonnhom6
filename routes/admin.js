var express = require('express');
var router = express.Router();
const conn = require('../connection')

//check login
router.use(function (req, res, next) {
    if (req.session.user) {
        if (!parseInt(req.session.user.auth)) {
            res.render('error', {
                error: true,
                message: "[YOU ARE NOT ADMIN]"
            });
        }
        else {
            next()
        }
    }
    else
        res.render('error', {
            error: true,
            message: "[YOU MUST LOGIN FIRST]"
        });
})
//index
// middleware
router.get("/", async (req, res, next) => {
    var sql = ` SELECT 
    COUNT(product_id) is_ok
FROM
    product 
group by is_ok
order by is_ok asc; `
    conn.query(sql, [], (err, result, field) => {
        if (err) {
            res.render("error", {
                error: true,
                message: err
            })
            throw err
        }
        var product_not_accept = 0, product_accept = 0
        if (result[0]) {
            product_accept = result[0].is_ok
        }
        if (result[1]) {

            product_not_accept = result[1].is_ok
        }
        req.body = {
            ...req.body,
            product_not_accept: product_not_accept,
            product_accept: product_accept
        }
        next()
    })
})
router.get("/", async (req, res, next) => {
    var sql = `Select count(user_id) num_user from user;`

    await conn.query(sql, [], (err, result, field) => {
        if (err) {
            res.render("error", {
                error: true,
                message: err
            })
            throw err
        }
        req.body = {
            ...req.body,
            num_user: result[0].num_user
        }
        next()
    })
})
router.get("/", async (req, res, next) => {
    var sql = `SELECT 
    COUNT(aucutionn_id) num_aucution
FROM
    product_aucution
WHERE time_down<'${new Date().getTime()}'
AND is_ok = 1;
`

    conn.query(sql, [], (err, result, field) => {
        if (err) {
            res.render("error", {
                error: true,
                message: err
            })
            throw err
        }
        req.body = {
            ...req.body,
            num_aucution: result[0].num_aucution
        }
        next()
    })
})
router.get("/", (req, res, next) => {
    res.render('admin', {
        user: req.session.user,
        type: 'profile',
        product_not_accept: req.body.product_not_accept,
        product_accept: req.body.product_accept,
        num_user: req.body.num_user,
        num_aucution: req.body.num_aucution
    })
    res.end()
})
// end index

// check login
router.get('/register', (req, res, next) => {
    if (req.session.user) {
        return res.redirect("/",
            {
                error: true,
                message: "[YOU ARE LOGINED]"
            }
        )
    }
    else res.render('register', { message: "REGISTER ADMIN now!".toUpperCase(), user: null })
})
//register
router.post('/register', function (req, res, next) {
    if (req.session.user) {
        console.log({
            error: true,
            message: "[YOU ARE LOGINED!!]"
        })
        return res.redirect('/')
    }
    // set biến auth = 1 và chuyển dữ liệu tới router /register
    req.app.set('auth', '1')
    res.redirect(307, '/register')
})

//xem danh sách người dùng
//middle ware

router.get('/allUser', async function (req, res, next) {
    sql = `SELECT 
    u.*, COUNT(p.product_id) num_of_product
FROM
    user u
        LEFT JOIN
    product p ON u.user_id = p.user_id
GROUP BY u.user_id;`
    await conn.query(sql, [], (err, result, field) => {
        if (err) {
            res.render('error', {
                error: true,
                message: err
            })
        }
        else {
            req.body = {
                ...req.body,
                listUser: result
            }
        }
        next()
    })
})
router.get('/allUser', function (req, res, next) {
    sql = `SELECT 
     u.*, COUNT(pa.aucutionn_id) num_of_aucution
FROM
    user u,
    product_aucution pa
WHERE
    u.user_id = pa.user_id
    AND pa.is_ok = 1
    AND pa.time_down <= ${new Date().getTime()}
GROUP BY u.user_id;`
    conn.query(sql, [], (err, result, field) => {
        if (err) {
            res.render('error', {
                error: true,
                message: err
            })
        }
        else {
            req.body.listUser.forEach((v, i) => {
                v.num_of_aucution = 0;
                result.forEach((value, index) => {
                    if (value.user_id == v.user_id) {
                        v.num_of_aucution = value.num_of_aucution;
                        return;
                    }
                })
            })
            res.render('admin', {
                error: false,
                message: "[Get all user OK]",
                listUser: req.body.listUser,
                type: "allUser",
                user: req.session.user
            })
        }
    })
})
// end xem danh sách người dùng



// xem các sản phẩm chờ xét duyệt
router.get('/product/accept/:accept', (req, res, next) => {

    var sql = `SELECT 
    p.*, u.username, u.full_name user_full_name,u.phone, u.address, c.category_name
FROM
    product p,
    user u,
    category c
WHERE
    p.user_id = u.user_id
    AND c.category_id = p.category_id   
    AND p.is_ok = ${req.params.accept}
ORDER BY p.time_up ASC;`
    conn.query(sql, [], (err, result, field) => {
        if (err) {
            res.render('error', {
                error: true,
                message: err
            })
        }
        else {
            console.log("[Xem các sản phẩm" + req.params.accept == 1 ? "productAccept" : "productNoAccept" + " OK], length: " + result.length);

            res.render("admin", {
                is_accept: req.params.accept,
                error: false,
                listProduct: result,
                type: req.params.accept == 1 ? "productAccept" : "productNoAccept",
                user: req.session.user,
                message: req.params.accept == 1 ? "LIST PRODUCT ACCEPT" : "LIST PRODUCT NOT ACCEPT",
                serverTime: new Date().getTime()
            })
        }
    })
})
//xét duyệt sản phẩm
//từ chối sản phẩm
// xóa sản phẩm
router.post('/product/action/:pID', (req, res, next) => {
    var action = req.body.action
    var sql = null
    if (action == 'delete')
        sql = `DELETE FROM product WHERE (product_id = '${req.params.pID}' AND user_id = '${req.body.user_id}');`
    else if (action == 'accept')
        sql = `UPDATE product SET is_ok = '1' WHERE (product_id = '${req.params.pID}' AND user_id = '${req.body.user_id}');`
    else if (action == 'decline')
        sql = `UPDATE product SET is_ok = '0' WHERE (product_id = '${req.params.pID}' AND user_id = '${req.body.user_id}');`
    conn.query(sql, [], (err, result, field) => {
        if (err) {
            console.log("[" + action + " FALUIRE]");
            res.render("error", {
                error: true,
                message: err
            })
        }
        else {
            console.log("[" + action + " OK]");
            res.send({
                error: false,
                message: "[" + action + " OK]"
            })
        }
    })
})

module.exports = router;
