var express = require('express');
var router = express.Router();
const conn = require('../connection')

// xem sản phẩm theo category
router.get('/', function (req, res, next) {
   var cID = req.query.category
   var sql = `SELECT 
    p.*, u.username, u.full_name user_full_name,u.phone, u.address, c.category_name
FROM
    product p,
    user u,
    category c
WHERE
    p.user_id = u.user_id
    AND c.category_id = p.category_id
    AND p.category_id = ${cID}
    AND p.is_ok = '1'
ORDER BY p.time_up ASC; `
   conn.query(sql, [], (err, result, field) => {
      if (err) {
         res.send({
            error: true,
            message: err
         })
         throw err
      }
      else {
         console.log("[Get all product OK]");

         req.session.search.selected_category_id = cID
         if (req.query.type == 0) {
            res.send({
               error: false,
               product: result,
        serverTime: new Date().getTime()
            })
         }
         // xem sản phầm liên quan theo category
         else {
            res.render('categoryProduct', {
               ...req.session.search,
               category: req.session.category,
               result: result,
               num: result.length,
               user: req.session.user || null,
               message: "[Get all product OK]",
        serverTime: new Date().getTime()
            });
         }
      }
   })
});

module.exports = router;
