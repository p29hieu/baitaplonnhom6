var express = require("express");
var router = express.Router();
const conn = require("../connection");

//db giống index.js thì num =1; còn giống catpro thi =0
router.get("/:pID", function (req, res, next) {
  var pID = req.params.pID;
  var sql = `SELECT 
   p.*, u.username, u.full_name user_full_name,u.phone, u.address, c.category_name
FROM
   product p,
   user u,
   category c
WHERE
   p.user_id = u.user_id
   AND c.category_id = p.category_id
   AND p.product_id = ${pID}
ORDER BY p.time_up DESC; `;
  conn.query(sql, [], (err, result, field) => {
    if (err) {
      res.send({
        error: true,
        message: err,
      });
      throw err;
    }
    else if (result.length) {
      console.log("[Get product OK id: " + pID + "]");
      req.session.search.selected_category_id = result[0].category_id;
      res.render("single", {
        ...req.session.search,
        category: req.session.category,
        user: req.session.user,
        result: result[0],
        message: "[Get product OK id: " + pID + "]",
      });
    }
    else {
      console.log("[not found product id: " + pID + "]");
      res.render("single", {
        ...req.session.search,
        category: req.session.category,
        user: req.session.user,
        result: null,
        message: "[not found product id: " + pID + "]",
      });
    }
  });
});

module.exports = router;
