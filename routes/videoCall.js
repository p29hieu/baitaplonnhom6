var express = require('express');
const conn = require('../connection')
var router = express.Router();
var formidable = require('formidable');
var fs = require('fs');
const getAccessToken = require('../accessToken/access_token_for_client')

// demo
router.get("/user1", (req, res, next) => {
    res.render("video-call-demo/user1",
        {}
    )
})
router.get("/user2", (req, res, next) => {
    res.render("video-call-demo/user2",
        {}
    )
})
// end demo

// get token and get new token
router.get("/getAccessToken/:userID", (req, res, next) => {
    let userID = req.params.userID
    // let userID = req.session.user.user_id
    var token = req.session.user.token
    if (req.query.new == 1) {
        token = getAccessToken(userID)
        req.session.user.token = token
    }
    res.send({
        error: false,
        token: token
    })
})
module.exports = router