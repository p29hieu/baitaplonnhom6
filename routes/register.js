var express = require('express');
var router = express.Router();
const conn = require('../connection')

router.get('/', (req, res, next) => {
    if (req.session.user) {
        console.log({
            error: true,
            message: "[YOU ARE LOGINED!!]"
        })

        return res.redirect('/')
    }
   else  res.render('register', { message: "Signup now!".toUpperCase(), user: null })
})
//check login
router.use(function (req, res, next) {
    var username = req.body.username
    var sql = `select * from \`user\` where username = '${username}';`
    conn.query(sql, [], (err, result, field) => {
        if (err) {
            res.render('error', {
                error: true,
                message: err,
                user: null
            })
            throw err
        }
        else if (result.length) {
            res.render('register', {
                error: true,
                message: "[Username is exists]",
                user: null
            })
        }
        else next();
    })
})

//Signup
router.post('/', function (req, res, next) {
    var user = {
        username: req.body.username,
        password: req.body.password,
        full_name: req.body.fullName,
        phone: req.body.phone,
        address: req.body.address,
        auth: req.body.auth || "0"
    }
    // lấy dữ liệu (từ phía đăng kí admin) từ router trước gửi sang
    if (req.app.get('auth'))
        user.auth = '1'
    var sql = ` INSERT INTO \`user\` (\`username\`, \`password\`, \`full_name\`, \`phone\`, \`address\`, \`auth\`) VALUES ('${user.username}', '${user.password}', '${user.full_name}', '${user.phone}', '${user.address}', '${user.auth}');`
    conn.query(sql, [], (err, result, fields) => {
        if (err) {
            res.render('error', {
                error: true,
                message: err,
                user: null
            })
            throw err
        }
        else {
            req.session.user = {
                ...user,
                user_id: result.insertId
            }
            console.log(`[REGISTER OK, WELCOME '${user.full_name}' - auth: ${user.auth}]`)
            res.redirect('/')

        }
    })

})

module.exports = router;