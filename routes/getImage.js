var express = require('express');
var router = express.Router();
var path = require('path');
router.get('/:name', (req, res) => {
    const fileName = req.params.name;
    // console.log(new Date(), '[getImage]: ', fileName);
    if (!fileName) {
        res.send({
            error: true,
            message: 'no filename specified',
        })
    }
    res.sendFile(path.resolve(`./images/${fileName}`), (err) => {
        console.log("[GET IMAGE ERROR]", err)
        res.end()
    });
})
module.exports = router;